#!/bin/sh

# phpcs path
PHPCS=tools/phpcs.phar

# custom coding standard file path
CUSTOM_PSR2=tools/custom-psr2.xml

# phpcs report
CS_REPORT_JSON=" --standard=$CUSTOM_PSR2 --report=json "

# staged files in Git
STAGED_FILES=$(git diff --name-only --staged | grep '\.php$')

getErrorCount() {
  if [ -f "$PHPCS" ];
  then
    cs_report_json=$(php $PHPCS $CS_REPORT_JSON $STAGED_FILES)
    errors_string=$(echo $cs_report_json | grep -o -E '"totals":{"errors":[0-9]{1,},')
    prefix='"totals":{"errors":'
    suffix=','
    errors_no=$(echo $(expr "$errors_string" : "$prefix\(.*\)$suffix"))

    if [ "$errors_no" \> 0 ]
    then
      echo $errors_no;
      exit 0
    fi
  else
    echo -1
    exit 1
  fi
  echo 0
}

runCsChecking(){
  	echo php $PHPCS --colors --report-width=120 --standard=$CUSTOM_PSR2 $STAGED_FILES
}